using System.Collections.Generic;
using System.IO;
using System.Linq;
using ApiServer.Controllers;
using ApiServer.Enums;
using ApiServer.Interfaces;
using ApiServer.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Newtonsoft.Json;

namespace ApiServerTests
{
    public class RealEstateControllerTests
    {
        private readonly RealEstateController _controller;
        private readonly Mock<IPropertyService> _svcMock;
        private readonly IEnumerable<Property> _properties;

        public RealEstateControllerTests()
        {
            _svcMock = new Mock<IPropertyService>();
            _controller = new RealEstateController(_svcMock.Object);

            var directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var path = Path.Combine(directory, "testProperties.json");
            var jsonData = File.ReadAllText(path);
            _properties = JsonConvert.DeserializeObject<List<Property>>(jsonData) ?? new List<Property>();
        }

        [SetUp]
        public void Setup()
        {
            _svcMock.Setup(x => x.GetAllProperties()).Returns(_properties);
        }

        [TearDown]
        public void TearDown()
        {
            _svcMock.Reset();
        }

        [Test]
        public void ShouldReturnPropertiesIfTypeExistsAndLocationIsInsideViewport()
        {
            var viewport = new ViewPort
            {
                High = new Location(47.34478320000000, 8.3531108),
                Low = new Location(47.34478319999998, 8.3531106)
            };

            var result = _controller.FindProperties(new FindPropertiesRequest
            {
                Type = RealEstateType.Apartment,
                ViewPort = viewport
            });

            var properties = ((OkObjectResult)result).Value as List<Property>;

            Assert.NotNull(properties);
            Assert.AreEqual(2, properties.Count);

            foreach (var property in properties)
            {
                Assert.AreEqual(RealEstateType.Apartment, property.RealEstateType);
                AssertIsInsideViewport(property, viewport);
            }
        }

        [TestCase(47.34478319999998, 8.3531106, 47.34478319999997, 8.3531105)]
        [TestCase(47.34478319999999, 8.3531107, 47.34478319999999, 8.3531107)]
        [TestCase(50, 50, 1, null)]
        [TestCase(null, null, null, null)]
        public void ShouldReturnNoPropertiesIfIsOutsideViewportOrOnEdge(double? hiLat, double? hiLng, double? lowLat,
            double? lowLng)
        {
            var viewport = new ViewPort
            {
                High = new Location(hiLat, hiLng),
                Low = new Location(lowLat, lowLng)
            };

            var result = _controller.FindProperties(new FindPropertiesRequest
            {
                Type = RealEstateType.Apartment,
                ViewPort = viewport
            });

            var properties = ((OkObjectResult) result).Value as List<Property>;

            Assert.NotNull(properties);
            Assert.AreEqual(0, properties.Count);
        }

        [Test]
        public void ShouldNotReturnPropertiesWithoutLocation()
        {
            var viewport = new ViewPort
            {
                High = new Location(100, 100),
                Low = new Location(1, 1)
            };

            var result = _controller.FindProperties(new FindPropertiesRequest
            {
                Type = RealEstateType.House,
                ViewPort = viewport
            });

            var properties = ((OkObjectResult)result).Value as List<Property>;

            Assert.NotNull(properties);

            foreach (var property in properties)
            {
                Assert.NotNull(property.Location.Latitude);
                Assert.NotNull(property.Location.Longitude);
            }
        }

        [Test]
        public void ShouldReturnPropertiesOfAllTypesIfTypeIsNone()
        {
            var viewport = new ViewPort
            {
                High = new Location(100, 100),
                Low = new Location(1, 1)
            };

            var result = _controller.FindProperties(new FindPropertiesRequest
            {
                Type = RealEstateType.None,
                ViewPort = viewport
            });

            var properties = ((OkObjectResult)result).Value as List<Property>;

            Assert.NotNull(properties);

            var apartments = properties.Where(x => x.RealEstateType == RealEstateType.Apartment);
            var houses = properties.Where(x => x.RealEstateType == RealEstateType.House);

            Assert.IsNotEmpty(apartments);
            Assert.IsNotEmpty(houses);
        }

        private void AssertIsInsideViewport(Property property, ViewPort viewport)
        {
            Assert.Greater(property.Location.Latitude, viewport.Low.Latitude);
            Assert.Less(property.Location.Latitude, viewport.High.Latitude);
            Assert.Greater(property.Location.Longitude, viewport.Low.Longitude);
            Assert.Less(property.Location.Longitude, viewport.High.Longitude);
        }
    }
}