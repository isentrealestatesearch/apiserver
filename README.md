## What does it do

Find real estate properties filtered by type and google maps viewport.

## How to run

Open project and run. Start page is swagger with data contracts descrition.

Api endpoint: https://localhost:7108/RealEstate

## Assumptions
 1. Properties that doesn't have location should not be displayed.
 2. Users may want to see more detailed info on markers than just a title, hence all property data is exposed by the endpoint. 

## Future improvements

 1. More unit tests.
 2. Integration tests.
 3. Json normalization. Since initial json was invalid I applied a convention that if location doesn't have latitude or longitude values they should be set as `null` instead of an empty string. Assuming that json data is provided by 3d parties it's hard to force them to follow one convetion when creating data files. Hence, the app can have a step, which converts json provided by 3d parties to valid json, which complies with the above convention (and possibly other). This step can be run before reading json from disk or, ideally, once, when 3d parties send a new json.

 4. More filter options.
 5. Region filter by city, district, canton (is we're talking only about Switzerland), etc.
