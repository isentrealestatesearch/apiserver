﻿namespace ApiServer.Enums
{
    public enum RealEstateType
    {
        None,
        Apartment,
        House
    }
}
