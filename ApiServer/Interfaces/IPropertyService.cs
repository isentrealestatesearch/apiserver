﻿using ApiServer.Models;

namespace ApiServer.Interfaces
{
    public interface IPropertyService
    {
        IEnumerable<Property> GetAllProperties();
    }
}
