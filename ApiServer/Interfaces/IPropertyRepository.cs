﻿using ApiServer.Models;

namespace ApiServer.Interfaces
{
    public interface IPropertyRepository
    {
        IEnumerable<Property> GetAll();
    }
}
