﻿namespace ApiServer.Models
{
    public class ViewPort
    {
        public Location Low { get; set; }

        public Location High { get; set; }
    }
}
