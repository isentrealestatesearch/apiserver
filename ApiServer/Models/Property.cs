﻿using ApiServer.Enums;

namespace ApiServer.Models
{
    public class Property
    {
        public string Title { get; set; }

        public RealEstateType RealEstateType { get; set; }

        public string StreetName { get; set; }

        public string StreetNumber { get; set; }

        public int Zip { get; set; }

        public string City { get; set; }

        public Location Location { get; set; }
    }
}
