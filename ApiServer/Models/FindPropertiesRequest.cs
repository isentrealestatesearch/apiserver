﻿using ApiServer.Enums;

namespace ApiServer.Models
{
    public class FindPropertiesRequest
    {
        public RealEstateType Type { get; set; }

        public ViewPort ViewPort { get; set; }
    }
}
