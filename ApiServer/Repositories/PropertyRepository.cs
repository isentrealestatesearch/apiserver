﻿using ApiServer.Helpers;
using ApiServer.Interfaces;
using ApiServer.Models;
using Newtonsoft.Json;

namespace ApiServer.Repositories
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly string _filePath;

        public PropertyRepository(IWebHostEnvironment environment, IConfiguration config)
        {
            _filePath = Path.Combine(environment.ContentRootPath, config[Constants.ConfigJsonDataPath]);
        }

        public IEnumerable<Property> GetAll()
        {
            var jsonData = File.ReadAllText(_filePath);
            var properties = JsonConvert.DeserializeObject<List<Property>>(jsonData) ?? new List<Property>();

            return properties;
        }
    }
}
