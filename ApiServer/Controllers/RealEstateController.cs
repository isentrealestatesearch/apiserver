using ApiServer.Enums;
using ApiServer.Interfaces;
using ApiServer.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RealEstateController : ControllerBase
    {
        private readonly IPropertyService _propertyService;

        public RealEstateController(IPropertyService propertyService)
        {
            _propertyService = propertyService;
        }

        // POST: RealEstate
        [HttpPost]
        public IActionResult FindProperties(FindPropertiesRequest request)
        {
            var properties = _propertyService.GetAllProperties();

            if (request.Type != RealEstateType.None)
            {
                properties = properties.Where(x => x.RealEstateType == request.Type);
            }

            properties = properties.Where(x => x.Location.Latitude > request.ViewPort.Low.Latitude &&
                                               x.Location.Latitude < request.ViewPort.High.Latitude &&
                                               x.Location.Longitude > request.ViewPort.Low.Longitude &&
                                               x.Location.Longitude < request.ViewPort.High.Longitude
            ).ToList();

            return Ok(properties);
        }
    }
}