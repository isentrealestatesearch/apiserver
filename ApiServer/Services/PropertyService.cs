﻿using ApiServer.Interfaces;
using ApiServer.Models;

namespace ApiServer.Services
{
    public class PropertyService : IPropertyService
    {
        private readonly IPropertyRepository _propertyRepository;

        public PropertyService(IPropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }

        public IEnumerable<Property> GetAllProperties()
        {
            return _propertyRepository.GetAll();
        }
    }
}
